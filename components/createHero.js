



import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, TextInput, TouchableOpacity, Modal,TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2, ListItem, CheckBox, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LinearGradient from 'react-native-linear-gradient';

var axios = require('axios');

var unchecked = require('../components/assets/checkbox.png');
var checked = require('../components/assets/checked.png');

export default class createHero extends Component {

    componentWillMount(){
        let self = this;

        AsyncStorage.getItem("login_arn").then((value) => {
            self.setState({login_arn: value});
            console.log(self.state.login_arn);
        }).done();

        AsyncStorage.getItem("newhero_arn").then((value) => {
            self.setState({create_arn: value});
            console.log(self.state.create_arn);
            
        }).done();
    }

    constructor(props){
        super(props)

        this.state = {
            firstName:'',
            first_name_style: styles.input_style,
            lastName:'',
            last_name_style: styles.input_style,
            emailAddress:'',
            email_style: styles.input_style,
            password:'',
            password_style: styles.input_style,
            confirm:'',
            confirm_style: styles.input_style,
            missing_info:'',
            modalVisible: false,
            modal2Visible: false,
            showCheck1: false,
            showCheck2: false,
            check_message_1:'',
            check_message_2:'',
        }
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
      }
      setModal2Visible(visible) {
        this.setState({modal2Visible: visible});
      }

    createAccount(){


        let self = this;

        let errors = false;

        if(this.state.showCheck1 == false){
            this.setState({check_message_1: '*You need to be over 18 and have the right to work in the uk.'});
        }else{
            this.setState({check_message_1:''});
        }

        if(this.state.showCheck2 == false){
            this.setState({check_message_2: '*You need to agree to the terms and conditions'});
        }else{
            this.setState({check_message_2:''});
        }

        if(self.state.firstName.length < 2 ){
            this.setState({missing_info: '*Missing Information'});
            this.setState({first_name_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({first_name_style: styles.input_style})
        }

        if(self.state.lastName.length < 2 ){
            this.setState({last_name_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({last_name_style: styles.input_style})
        }

        if(self.state.emailAddress.search('@') === -1){
            this.setState({email_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({email_style: styles.input_style})
        }

        if(self.state.password < 7){
            this.setState({password_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({password_style: styles.input_style})
        }

        if(self.state.confirm < 7){
            this.setState({confirm_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({confirm_style: styles.input_style})
        }

        if(self.state.password !== self.state.confirm || self.state.password === ''){
            this.setState({confirm_style: styles.error});
            errors = true
        }else {
            this.setState({missing_info:''})
            this.setState({confirm_style: styles.input_style})
        }

        if(errors) {
            this.setState({missing_info: '*Missing Information'});
        }else {

            axios.post(self.state.create_arn,
            {
                service:'client',
                firstname: self.state.firstName,
                lastname: self.state.lastName,
                email: self.state.emailAddress,
                pw: self.state.password,
                code: 'tube10',
                regtype: 1,
                source:'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MASM; .NET4.0C; .NET4.0E; rv:11.0) like Gecko',
                json:true
            }).then((response) => {

                var status = JSON.parse(response.data.response.status);

                if(status == '200'){

                    axios.post(self.state.login_arn, { service:'client', user:self.state.emailAddress, pw:self.state.password, json:true })
                    .then((response) =>{
                        var status = JSON.parse(response.data.response.status);
                        var sessionid = response.data.response.result.sessionid;

                        AsyncStorage.setItem('sessionid', sessionid);
                            Actions.homepage();
                    });

                }else if(status == '404'){
                    var error_message = '*' + response.data.response.message;
                self.setState({missing_info: (error_message)});
                }

            })

    }

}

    renderImage1() {
        var imgSource = this.state.showCheck1 ? checked : unchecked;
        
            return (
                <Image source={ imgSource } />
            )
    }

    renderImage2() {
        var imgSource = this.state.showCheck2 ? checked : unchecked;
        return (
          <Image source={ imgSource } />
        );
    }

    render() {

        return (
            <ImageBackground source={require('./assets/bg_320.png')} style={{flex: 1,width: null,height: null,}} resizeMode="cover">
            <Container style={ styles.container }>

                <Content style={ styles.content }>
                            <Button transparent light style={styles.back_arrow_container} onPress={() => Actions.login()}>
                                <Image source={require('./assets/back_arrow.png')} />
                            </Button>

                            <H2 style={ styles.headerTextOne }>Sign In</H2>
                            <Text style={ styles.headertext }>Some basic details would be fab.</Text>

                    <Modal animationType="fade" transparent={false} visible={this.state.modalVisible} onRequestClose={() => {alert('Modal has been closed.');}}>
                        <View style={{marginTop: 22}}>
                            <Grid>
                                <Row>
                                    <Col>
                                        <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                                            <Text style={styles.modal_close}>X</Text>
                                        </TouchableHighlight>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Text style={{fontSize:25, textAlign:'center', marginTop:70, color:'#333558'}}>Terms And Conditions</Text>
                                        <Text style={styles.modal_text}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                    </Modal>
                    <Modal animationType="fade" transparent={false} visible={this.state.modal2Visible} onRequestClose={() => {alert('Modal has been closed.');}}>
                        <View style={{marginTop: 22}}>
                            <Grid>
                                <Row>
                                    <Col>
                                        <TouchableHighlight onPress={() => {this.setModal2Visible(!this.state.modal2Visible);}}>
                                            <Text style={styles.modal_close}>X</Text>
                                        </TouchableHighlight>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Text style={{fontSize:25, textAlign:'center', marginTop:70, color:'#333558'}}>Right To Work Check</Text>
                                        <Text style={{textAlign:'left', fontSize:16,paddingTop:20}}>You will need to have an original copy of documents to prove your right to work in the UK</Text>
                                        <Text style={{textAlign:'center', fontSize:16,paddingTop:20}}>For British applicants:</Text>
                                        <Text style={{textAlign:'left', fontSize:16,paddingTop:20}}>British passport (doesn't matter if this has expired) Birth certificate, proof of National insurance number and photo ID</Text>
                                        <Text style={{textAlign:'center', fontSize:16,paddingTop:20}}>For European applicants:</Text>
                                        <Text style={{textAlign:'left', fontSize:16,paddingTop:20}}>EU ID card (must be in date or within 5 yrs of expiring) EU passport (must be in date or within 5 yrs of expiring) </Text>
                                        <Text style={{textAlign:'center', fontSize:16,paddingTop:20}}>For applicants from outside Europe:</Text>
                                        <Text style={{textAlign:'left', fontSize:16,paddingTop:20}}>British Residence Permit (in date) and your passport UK Visa (must be in date) and your passport</Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                    </Modal>
                    <Grid>
                        <Row>
                            <Col style={{padding:5}}>
                                <TextInput 
                                style={this.state.first_name_style} 
                                placeholder='First Name' 
                                placeholderTextColor='#B3B3B3' 
                                onChangeText={(text) => this.setState({firstName:text})} 
                                />
                            </Col>
                            <Col style={{padding:5}}>
                                <TextInput 
                                style={this.state.last_name_style} 
                                placeholder='Last Name' 
                                placeholderTextColor='#B3B3B3' 
                                onChangeText={(text) => this.setState({lastName:text})} 
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5}}>
                                <TextInput style={this.state.email_style} placeholder='Email Address' placeholderTextColor='#B3B3B3' onChangeText={(text) => this.setState({emailAddress:text})} />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5}}>
                                <TextInput style={this.state.password_style} placeholder='Password' placeholderTextColor='#B3B3B3' secureTextEntry={true} onChangeText={(text) => this.setState({password:text})} />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5, marginBottom:0}}>
                                <TextInput style={this.state.confirm_style} placeholder='Confirm Password' placeholderTextColor='#B3B3B3' secureTextEntry={true} onChangeText={(text) => this.setState({confirm:text})} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Text style={styles.error_message_style}>
                                    { this.state.missing_info }
                                </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{width:320}}>
                                <ListItem style={ styles.listitem1 }>
                                    <TouchableOpacity onPress={() => this.setState({ showCheck1: !this.state.showCheck1 })} >
                                        { this.renderImage1() }
                                    </TouchableOpacity>
                                    <Body>
                                        <Text style={ styles.checktext }>Im over 18 and have the right to work in the UK</Text>
                                    </Body>
                                </ListItem>
                            </Col>
                            <Col>
                                <TouchableHighlight onPress={() => {this.setModal2Visible(true);}} style={{height:15, width:150,marginBottom:10}}>
                                    <Image source={require('./assets/info_icon.png')} style={{marginLeft:-55}} />
                                </TouchableHighlight>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <ListItem style={ styles.listitem }>
                                    <TouchableOpacity onPress={() => this.setState({ showCheck2: !this.state.showCheck2 })} >
                                        { this.renderImage2() }
                                    </TouchableOpacity>
                                    <Body>
                                        <Text style={ styles.checktext }>
                                            <Text style={{color:'white', width:100, fontSize:14}}>Agree to </Text>
                                            <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{height:15, width:150,marginBottom:10}}>
                                                <Text style={{textDecorationLine:'underline', color:'white'}}>Terms & Conditions</Text>
                                            </TouchableHighlight>
                                        </Text>
                                    </Body>
                                </ListItem>

                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <Text style={styles.error_message_style_2}>
                                {this.state.check_message_1}
                            </Text>
                            <Text style={styles.error_message_style_2}>
                                {this.state.check_message_2}
                            </Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5}}>
                            <LinearGradient start={{x: 0, y: 0.75}} end={{x: 1, y: 0.25}} colors={['#2af5ff', '#7c42fb']} style={styles.logInGradient}>
                                <TouchableOpacity style={styles.button} onPress={() => this.createAccount()} >
                                        <Text style={styles.buttonText}>Next</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
            </ImageBackground>
        )
    }
}

AppRegistry.registerComponent('createHero', () => createHero);

const styles = StyleSheet.create({

    input_style:{
        height:40,
        backgroundColor:'#5d5e7f',
        borderRadius:5,
        paddingLeft:10,
        color:'white'
    },
    error:{
        height:40,
        backgroundColor:'#5d5e7f',
        borderRadius:5,
        paddingLeft:10,
        color:'white',
        borderWidth:1,
        borderColor:'#FD7A10'
    },
    modal_text:{
        textAlign:'left',
        padding:20,
        color:'#929292',
        fontSize:18,
    },
    modal_close:{
        fontSize:30,
        textAlign:'right',
        padding:20,
        paddingRight:10,
        marginBottom:40
    },
    container:{
        borderWidth:0,
        padding:10
    },
    content:{
        height:500
    },
    headerTextOne:{
        textAlign:'center',
        marginTop:30,
        color:'white'
    },
    headertext:{
        textAlign:'center',
        marginTop:10,
        marginBottom:20,
        color:'white'
    },
    checktext:{
        color:'#fff',
        fontSize:14
    },
    buttonText:{
        marginTop:10,
        textAlign:'center',
        color:'white'
    },
    button:{
        height:40,
        borderRadius:5
    },
    logInGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop:10
      },
      logInGradientForm: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        margin:5,
        marginTop:10
      },
      listitem1:{
        marginRight:10,
        borderBottomWidth:0,
        marginTop:-15,
    },
    listitem:{
        marginRight:10,
        borderBottomWidth:0
    },
    checkbox_img:{
        marginLeft:15
    },
    back_arrow_container:{
        width:60,
        height:40,
        top:33,
        left:5,
        position:'absolute',
        zIndex:2,
    },
    error_message_style:{
        color:'#2af5ff',
        marginLeft:10,
    },
    error_message_style_2:{
        color:'#2af5ff',
        marginLeft:20,
        fontSize:14,
        marginTop:5
    }


})