



import {observable} from 'mobx'

class appStore {
    @observable filter = ""
}

var store = new appStore

export default store

