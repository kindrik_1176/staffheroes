



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2, ListItem, CheckBox, Body, Col, Grid, Row } from 'native-base';

export default class wizard_start extends Component {

    constructor(props){
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <Container style={ styles.container }>
                <Content style={ styles.content }>
                <Text style={ styles.close_wizard}>X</Text>
                <Text style={ styles.header}>Complete Your Profile</Text>
                <Row style={ styles.row_style }>
                    <View style={ styles.profile_image}></View>
                </Row>
                <Text style={ styles.header}>Hi Josh,</Text>
                <Text style={ styles.header_content}>This wizard will help you set up your account in a few easy steps. It will take up to 10 minutes.</Text>
                <Text style={ styles.header_content}>So grab a cup of tea, and lets start with your photo.</Text>
                <Text style={ styles.header_footnote}>You can always skip the wizard and complete your account later in the My Profile section.</Text>
                <Button style={ styles.nextButton } onPress={() => Actions.wizard_phone()} block>
                    <Text>NEXT</Text>
                </Button>
                <Row style={ styles.row_style}>
                    <View style={ styles.bottom_nav_active}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                </Row>
                </Content>
            </Container>
        )
    }
}

AppRegistry.registerComponent('wizard_start', () => wizard_start);

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        borderWidth:0,
        padding:10
    },
    content:{
        backgroundColor:'#fff',
        height:500
    },
    close_wizard:{
        alignItems:'flex-end',
        color:'#000',
        marginRight:20,
        fontSize:24,
        marginLeft:'auto'
    },
    header:{
        fontSize:24,
        color:'#000',
        textAlign:'center',
        marginTop:30
    },
    header_content:{
        fontSize:16,
        color:'#000',
        textAlign:'center',
        marginTop:30,
        marginLeft:20,
        marginRight:20
    },
    header_footnote:{
        fontSize:12,
        color:'blue',
        textAlign:'center',
        marginTop:30,
        marginLeft:20,
        marginRight:20
    },
    profile_image:{
        height:100,
        width:100,
        borderRadius:100/2,
        borderWidth:1,
        marginTop:30,
        borderColor:'#000'
    },
    nextButton:{
        borderRadius:0,
        marginTop:20,
        marginLeft:3,
        marginRight:1
    },
    bottom_nav_active:{
        height:20,
        width:20,
        borderRadius:20/2,
        backgroundColor:'blue',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    bottom_nav:{
        height:10,
        width:10,
        borderRadius:10/2,
        backgroundColor:'blue',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    row_style:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginLeft:3,
        marginRight:1
    },
    bottom_nav_remaining:{
        height:10,
        width:10,
        borderRadius:10/2,
        backgroundColor:'#e6f3f7',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
})