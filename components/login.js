



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, TextInput, TouchableOpacity, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2 } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LinearGradient from 'react-native-linear-gradient';

export default class login extends Component {

    constructor(props){
        super(props)

        this.state = {
            username:"",
            password:"",
            status: false,
            sessionid:''
        }
    }

    componentWillMount(){
        let self = this;

        AsyncStorage.getItem("sessionid").then((value) => {

            self.setState({sessionid:value});

            if(self.state.sessionid == null){

                axios.post('https://p3qujx9wga.execute-api.eu-west-2.amazonaws.com/dev/getAPI', { service:'client' })
                .then(function(response){
                    // checked, call is good.
                    console.log(response);
                    var url = JSON.parse(response.data.response.result.prod)[0];
                    // checked, url is good.
                    self.setState({
                        login_arn: url + 'loginhero',
                        newhero_arn: url + 'newhero',
                        homepage_arn: url + 'homepage',
                        forgot_password: url + 'forgotpassword',
                        availability_arn: url + 'availability'
                    });
                    //checked, all states above have been set.
                    AsyncStorage.setItem("login_arn", url + 'loginhero');
                    AsyncStorage.setItem("newhero_arn", url + 'newhero');
                    AsyncStorage.setItem("homepage_arn", url + 'homepage');
                    AsyncStorage.setItem("forgot_password", url + 'forgotpassword');
                    AsyncStorage.setItem("availability_arn", url + 'availability');
                    //checked, this sets the above arn's in async storage.
                });
            }else {
                Actions.availability();
            }
        }).done();

    }

    login(){
        let self = this;

        axios.post(self.state.login_arn, { service:'client', user:self.state.username, pw:self.state.password, json:true })
        .then((response) =>{
            var status = JSON.parse(response.data.response.status);
            //checked, status is correct.

            if(status == '200'){
                var sessionid = response.data.response.result.sessionid;
                //checked, session id is correct.
                AsyncStorage.setItem('sessionid', sessionid);
                Actions.homepage();
            }else if(status == '404'){
                var error_message = '*' + response.data.response.message;
                self.setState({error: (error_message)});
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('./assets/bg_360.png')} style={{flex: 1,width: null,height: null,}} resizeMode="cover">
             <View>
                <StatusBar barStyle="light-content" />
            </View>
            <Container style={ styles.container }>

                    <Content style={ styles.content }>

                        <Text style={ styles.headerTextOne }>Staff Heroes</Text>
                        <Text style={ styles.headertext }>The best way to find temp jobs</Text>

                        <Grid>
                            <Row>
                                <Col style={{padding:5}}>
                                    <TextInput style={styles.input_style} placeholder='Email Address' placeholderTextColor='#B3B3B3' onChangeText={(text) => this.setState({username:text})} />
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{padding:5}}>
                                    <TextInput style={styles.input_style} placeholder='Password' placeholderTextColor='#B3B3B3' secureTextEntry={true} onChangeText={(text) => this.setState({password:text})} />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Text style={styles.error}>{this.state.error}</Text>
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{padding:5, marginTop:30}}>
                                    <LinearGradient start={{x: 0, y: 0.75}} end={{x: 1, y: 0.25}} colors={['#ffb40b', '#fc4615']} style={styles.logInGradient}>
                                        <TouchableOpacity style={styles.button} onPress={() => this.login()} >
                                            <Text style={styles.buttonText}>Log in</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{padding:5}}>
                                    <Text style={ styles.forgotPass }  onPress={() => Actions.forgot_pass()}>Forgotten Password</Text>
                                    <View style={{ borderBottomColor:'#5d5e7f', borderBottomWidth:1, marginTop:40, margin:20 }} />
                                </Col>
                            </Row>
                            <Row>
                                <Col style={{padding:5}}>
                                <LinearGradient start={{x: 0, y: 0.75}} end={{x: 1, y: 0.25}} colors={['#2af5ff', '#7c42fb']} style={styles.createAccountGradient}>
                                    <TouchableOpacity style={styles.button} onPress={() => Actions.createHero()} >
                                        <Text style={styles.buttonText}>Create a new account</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                                </Col>
                            </Row>
                        </Grid>
                    </Content>

            </Container>
            </ImageBackground>
        )
    }
}

AppRegistry.registerComponent('login', () => login);

const styles = StyleSheet.create({

    input_style:{
        height:40,
        backgroundColor:'#5d5e7f',
        borderRadius:5,
        paddingLeft:10,
        color:'white'
    },
    button:{
        height:40
    },
    buttonText:{
        marginTop:10,
        textAlign:'center',
        color:'white'
    },
    container:{
        borderWidth:0,
        padding:10,
        flex:1
    },
    content:{
        flex:1
    },
    logInButton:{
        borderRadius:0,
        marginLeft:30,
        marginRight:1,
        backgroundColor:'transparent',
        flex:1
    },
    headerTextOne:{
        textAlign:'center',
        marginTop:50,
        color:'#fff',
        fontSize:34
    },
    headertext:{
        textAlign:'center',
        marginTop:10,
        color:'#fff',
        marginBottom:40
    },
    forgotPass:{
        textAlign:'center',
        marginTop:30,
        color:'#fff',
        fontStyle:'italic'
    },
    createButton:{
        borderRadius:0,
        marginLeft:3,
        marginRight:1,
        backgroundColor:'transparent'
    },
    logInGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop:-20
      },
      createAccountGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop:20
      },
      error:{
          textAlign:'left',
          color:'#2af5ff',
          marginTop:5,
          marginLeft:5

      }
})