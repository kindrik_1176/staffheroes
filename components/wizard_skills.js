



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2, ListItem, CheckBox, Body, Col, Grid, Row } from 'native-base';

export default class wizard_skills extends Component {

    constructor(props){
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <Container style={ styles.container }>
                <Content style={ styles.content }>
                    <Text style={ styles.close_wizard}>X</Text>
                    <Text style={ styles.header}>Complete Your Profile</Text>
                    <Text style={ styles.header}>About Your Skills</Text>

                <Button style={ styles.nextButton } onPress={() => Actions.wizard_payment()} block>
                    <Text>NEXT</Text>
                </Button>
                <Row style={ styles.row_style}>
                    <View style={ styles.bottom_nav}></View>
                    <View style={ styles.bottom_nav}></View>
                    <View style={ styles.bottom_nav}></View>
                    <View style={ styles.bottom_nav}></View>
                    <View style={ styles.bottom_nav_active}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                    <View style={ styles.bottom_nav_remaining}></View>
                </Row>
                </Content>
            </Container>
        )
    }
}

AppRegistry.registerComponent('wizard_skills', () => wizard_skills);

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        borderWidth:0,
        padding:10
    },
    content:{
        backgroundColor:'#fff',
        height:500
    },
    inputContainerOne:{
        borderWidth:0,
        borderColor:'#fff',
        marginTop:20
    },
    inputContainer:{
        borderWidth:0,
        borderColor:'#fff',
        marginTop:10
    },
    inputColor:{
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor:'#2699FB',
        height:200
    },
    input_number:{
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor:'#2699FB',
        color:'red',
        width:20,
        marginRight:5,
        textAlign:'center'
    },
    input_number_container:{
        borderWidth:0,
        borderColor:'#fff',
        marginTop:10,
        width:20
    },
    close_wizard:{
        alignItems:'flex-end',
        color:'#000',
        marginRight:20,
        fontSize:24,
        marginLeft:'auto'
    },
    header:{
        fontSize:24,
        color:'#000',
        textAlign:'center',
        marginTop:30
    },
    header_content:{
        fontSize:16,
        color:'#000',
        textAlign:'center',
        marginTop:30,
        marginLeft:20,
        marginRight:20
    },
    header_address:{
        fontSize:14,
        color:'#000',
        textAlign:'center',
        marginTop:30,
        marginLeft:20,
        marginRight:20
    },
    header_footnote:{
        fontSize:12,
        color:'blue',
        textAlign:'center',
        marginTop:30,
        marginLeft:20,
        marginRight:20
    },
    profile_image:{
        height:100,
        width:100,
        borderRadius:100/2,
        borderWidth:1,
        marginTop:30,
        borderColor:'#000'
    },
    search_button:{
        borderRadius:0,
        marginTop:20,
        marginLeft:3,
        marginRight:1
    },
    nextButton:{
        borderRadius:0,
        marginTop:20,
        marginLeft:3,
        marginRight:1
    },
    bottom_nav_active:{
        height:20,
        width:20,
        borderRadius:20/2,
        backgroundColor:'blue',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    bottom_nav:{
        height:10,
        width:10,
        borderRadius:10/2,
        backgroundColor:'blue',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    bottom_nav_remaining:{
        height:10,
        width:10,
        borderRadius:10/2,
        backgroundColor:'#e6f3f7',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    row_style:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginLeft:3,
        marginRight:1
    }
})