



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, TouchableOpacity, ScrollView, TouchableHighlight, FlatList} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2 } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Col, Row, Grid } from 'react-native-easy-grid';


export default class availability extends Component {



    constructor(props){
        super(props)

        this.state = {
            testArray:[],
            block_definition:[
                {block:'hours 1-4'},
                {block:'hours 4-8'},
                {block:'hours 8-12'},
                {block:'hours 12-16'},
                {block:'hours 16-20'},
                {block:'hours 20-24'}
            ]

        }

    }

    componentDidMount(){

        AsyncStorage.getItem("sessionid").then((value) => {
            this.setState({sessionid:value});
            console.log('session id', this.state.sessionid)
            
            AsyncStorage.getItem("availability_arn").then((value) => {

                function createdaystorender (today, availabilityrecords, nrofdays) {
    
                    function isthishourinthere (thedate, thehour, availabilityrecords) {               
                        let thedatehourtompare = new Date(thedate)
                        thedatehourtompare.setHours(thehour)
                        let shouldthisblockbeon = false                      
                        // its here we check if this block should be on
                        availabilityrecords.forEach(function (r) {
                            if (new Date(thedatehourtompare).getTime() >= new Date(r.fromdt).getTime() && new Date(thedatehourtompare).getTime() < new Date(r.todt).getTime() ) {
                                shouldthisblockbeon = true
                            } 
                        })               
                        return shouldthisblockbeon              
                    }             
                    function returnhoursforthisdate (forthisdate, availabilityrecords) {                       
                        let meh = []
                        for (var i=0; i<24; i++) {              
                            // check if this hour shall be true or false as in is in the set availability
                            let available = false              
                            if ( isthishourinthere (forthisdate, i, availabilityrecords))  {
                                available = true
                            }               
                            meh.push({
                                hour: i,
                                available: available
                            })
                        }                  
                        return meh   
                    }               
                    let fivedays = [];

                    for (var i=0; i<nrofdays; i++) {
                        fivedays.push({ourdate: today,
                            hours: returnhoursforthisdate (today, availabilityrecords)
                        })
                
                        let newdate = onlydate(new Date(new Date(today).getTime() + 60 * 60 * 24 * 1000))
                        today = newdate
                    }              
                    return(fivedays)
                };

                function onlydate (d) {
                    var theyear = d.getFullYear()
                    var themonth = d.getMonth() + 1
                    var thedate = d.getDate()                
                    if (themonth < 10) {
                        themonth = '0' + themonth
                    }                
                    if (thedate < 10) {
                        thedate = '0' + thedate
                    }
                    return theyear + '-' + themonth + '-' + thedate
                                  
                }
 
                this.setState({availability_arn:value});


                const final =  axios.post('https://lur5n5f6e2.execute-api.eu-west-2.amazonaws.com/dev/availability', {sessionid: this.state.sessionid, operation: 'getall'})
                .then((response) =>{

                    const availabilityRecords = response.data.response.result.blocks.records;
                    const blockDefinition = response.data.response.result.blockdefinition.records;

                    console.log('result',response)
                    
                    const test = availabilityRecords.forEach(function (el){

                    })

                    const startDate = new Date().getTime();

                    
                    let arrayofdaystorender = createdaystorender( onlydate(new Date()), availabilityRecords, 5 )



                    arrayofdaystorender.forEach(function (el,index,array) {
                        el.uniquekey= index+11111
                    })

                    this.setState({
                        testArray: arrayofdaystorender
                      })
                });

            }).done();

        }).done();

    }

    logout(){
        var terminate = '';
        AsyncStorage.setItem('sessionid', terminate);
        Actions.login();
    }

    render() {


        return (
            <Container style={ styles.container }>

                    <Content style={ styles.content }>
                    <Grid>
                        <ImageBackground source={require('./assets/header_image.png')} style={{flex: 1,width: null,height: null,}} resizeMode="cover">
                        <Row>
                            
                            <Col style={styles.header}>
                                <Text style={styles.header_text}>
                                    My Availability
                                </Text>
                            </Col>
                            
                        </Row>

                        <Row>
                            <Col style={styles.top_nav}>
                                <TouchableOpacity onPress={() => this.decrement()} >
                                    <Image source={require('./assets/back_arrow.png')} style={styles.left_arrow} />
                                </TouchableOpacity>
                            </Col>
                            <Col style={styles.top_nav}></Col>
                            <Col style={{width:150}}>
                                <Text style={styles.top_nav_date}>
                                    Current Month
                                </Text>
                            </Col>
                            <Col style={styles.top_nav}></Col>
                            <Col style={styles.top_nav}>
                                <TouchableOpacity onPress={() => this.logout()} >
                                    <Image source={require('./assets/forward_arrow.png')} style={styles.right_arrow} />
                                </TouchableOpacity>     
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styles.days_header_1}>
                                <Text style={{color:'#929292',fontSize:12,marginLeft:35}}>Tues</Text>
                                <Text style={{color:'#929292',fontSize:12,marginLeft:32}}>Wed</Text>
                                <Text style={{color:'#FC5C15',fontSize:12,marginLeft:25}}>Thurs</Text>
                                <Text style={{color:'#333558',fontSize:12,marginLeft:32}}>Fri</Text>
                                <Text style={{color:'#333558',fontSize:12,marginLeft:40}}>Sat</Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styles.days_header_2}>
                                <Text style={{color:'#929292',fontSize:20,marginLeft:40}}>27</Text>
                                <Text style={{color:'#929292',fontSize:20,marginLeft:30}}>28</Text>
                                <Text style={{color:'#FC5C15',fontSize:20,marginLeft:33}}>01</Text>
                                <Text style={{color:'#333558',fontSize:20,marginLeft:30}}>02</Text>
                                <Text style={{color:'#333558',fontSize:20,marginLeft:33}}>03</Text>
                            </Col>
                        </Row>
                        </ImageBackground>
                    </Grid>

                        <Grid>           
                        <Row>

                        <FlatList
                        keyExtractor={item => item.block}
                            data={this.state.block_definition}
                            renderItem={({ item }) => (
                                <View style={{height:44}}>
                                    <View style={{height:47, flex:1, flexDirection:'row',backgroundColor:'#F5F6F8'}}>

                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,marginLeft:-30}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>
                                            <View style={{borderWidth:1,borderColor:'black', height:40,width:50,marginRight:5,borderRadius:5,}}></View>

                                    </View>
                                </View>
                                
                            )}
                        />

                            {/*
                            {this.state.testArray.map((data, index) => 
                                (   
                                    <Grid key = {data.uniquekey}>
                                        <Row style={{backgroundColor:'white', height:50,flex:1,justifyContent:'center', alignItems:'center'}}>
                                            <Col style={{borderWidth:1, borderColor:'black', width:100, height:50}}>
                                                <Text style={{textAlign:'center', fontSize:12, color:'#333558'}}>
                                                    {data.ourdate}
                                                </Text>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col style={{backgroundColor:'#F5F6F8'}}>
                                                {data.hours.map((d, index) => 
                                                    (
                                                   <TouchableOpacity
                                                    key = {index}
                                                    activeOpacity={0.5}
                                                    onPress={()=>{}}
                                                    style={ d.available ? styles.pressed : styles.cube_container}
                                                    >
                                                        <Text style={{textAlign:'center'}}>
                                                            { d.available ?  'True' : 'False' }
                                                        </Text>
                                                    </TouchableOpacity>
                                                    
                                                    )
                                                )}
                                            </Col>
                                        </Row>
                                    </Grid>
                                )
                            )}
                        */}
                        </Row>

                        <Row>
                            <Col style={{height:65,backgroundColor:'#F5F6F8',justifyContent:'center'}}>
                                <Text style={{color:'#FC5C15',textAlign:'center'}}>Available hours:124</Text>
                            </Col>
                        </Row>

                    
                    </Grid>
                        
                    </Content>

                    <View style={styles.shadow_column}>
                        <Text style={styles.shadow_column_text}>2am - 6am</Text>
                        <Text style={styles.shadow_column_text}>6am - 10am</Text>
                        <Text style={styles.shadow_column_text}>10am - 2pm</Text>
                        <Text style={styles.shadow_column_text}>2pm - 6pm</Text>
                        <Text style={styles.shadow_column_text}>6pm - 10pm</Text>
                        <Text style={styles.shadow_column_text}>10pm - 2am</Text>
                    </View>
                    
            </Container>
        )
    }
}

AppRegistry.registerComponent('availability', () => availability);

const styles = StyleSheet.create({

    container:{
        borderWidth:0,
    },
    content:{
        height:600
    },
    header:{
        height:90,
    },
    header_text:{
        textAlign:'center',
        color:'white',
        fontSize:18,
        marginTop:40
    },
    top_nav:{
        height:40,
        flex:1,

    },
    left_arrow:{
        marginTop:12,
        alignSelf: 'flex-start',
        marginLeft:30

    },
    top_nav_date:{
        color:'white',
        textAlign:'center',
        marginTop:8,
        fontSize:16,
    },
    right_arrow:{
        marginTop:12,
        alignSelf: 'flex-end',
        marginRight:30
    },
    date_row:{
        height:60, 
        backgroundColor:'white'
    },
    shadow_column:{
        backgroundColor:'#929292', 
        position:'absolute', 
        top: 180, 
        bottom: 68, 
        left: 0, 
        right: 0, 
        width:60, 
        opacity:0.8,
        flex: 1,
        flexDirection: 'column', 
        justifyContent: 'space-around'
    },
    shadow_column_text:{
        color:'white', 
        textAlign:'center',
        fontSize:10
    },
    cube_container:{
        backgroundColor:'#E6E6E6',
        borderRadius:5,
        height:45,
        width:54,
        margin:5,
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    pressed:{
        backgroundColor:'#fd7a10',
        borderRadius:5,
        height:45,
        width:54,
        margin:5,
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    days_header_1:{
        height:25,
        flex:1, 
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#F5F6F8'
    },
    days_header_2:{
        height:25,
        flex:1, 
        flexDirection:'row', 
        alignItems:'center',
        backgroundColor:'#F5F6F8',
    },
    days_text:{
        color:'#333558',
        fontSize:12,
        marginLeft:40
    },
    days_date:{
        color:'#333558',
        fontSize:20,
        marginLeft:40
    }


})