



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2 } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Col, Row, Grid } from 'react-native-easy-grid';


export default class my_jobs extends Component {

    constructor(props){
        super(props)

        this.state = {

        }
    }


    render() {
        return (
            <Container style={ styles.container }>

                    <Content style={ styles.content }>
                        <H2 style={ styles.headerTextOne }>My Jobs Page</H2>
                    </Content>



            </Container>
        )
    }
}

AppRegistry.registerComponent('my_jobs', () => my_jobs);

const styles = StyleSheet.create({
    container:{
        borderWidth:0,
        padding:10
    },
    content:{
        height:600
    },
    slider:{
        height:200,
        borderColor:'black',
        borderWidth:1,

    },
    main_content:{

    },
    footer_text:{
        fontSize:12
    },
    image:{
        marginBottom:5
    },
    headerTextOne:{
        textAlign:'center',
        marginTop:100,
        color:'#000'
    },

})