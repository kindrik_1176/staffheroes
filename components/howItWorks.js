



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2, ListItem, CheckBox, Body } from 'native-base';

export default class howItWorks extends Component {

    constructor(props){
        super(props)

        this.state = {
            checkBoxChecked1: false,
            checkBoxChecked2: false,
            checkBoxChecked3: false
        }
    }

      onCheckBoxPress2() {
        console.log( "Checked!" );
        this.setState({
          checkBoxChecked2: !this.state.checkBoxChecked2
        });
      }

      onCheckBoxPress3() {
        console.log( "Checked!" );
        this.setState({
          checkBoxChecked3: !this.state.checkBoxChecked3
        });
      }



    render() {
        return (
            <Container style={ styles.container }>
                <Content style={ styles.content }>
                    <H2 style={ styles.headerTextOne }>See How It Works</H2>
                    <Text style={ styles.headerTextTwo}>Watch Video</Text>
                    <Text style={ styles.headerTextTwo}>And</Text>
                    <H2 style={ styles.headerTextTwo}>Join Staff Heroes</H2>
                    <Text style={ styles.headerTextThree}>Set up your account and start working today.</Text>
                    <Text style={ styles.headerTextFour}>You must be over 18 and have the right to work in the UK.</Text>
                        <ListItem style={ styles.listitem }>
                            <CheckBox style={ styles.checkbox} checked={this.state.checkBoxChecked2} onPress={()=>this.onCheckBoxPress2()} />
                            <Body>
                                <Text style={ styles.checktext }>I Am 18 Or Older</Text>
                            </Body>
                        </ListItem>
                        <ListItem style={ styles.listitem }>
                            <CheckBox style={ styles.checkbox} checked={this.state.checkBoxChecked3} onPress={()=>this.onCheckBoxPress3()} />
                            <Body>
                                <Text style={ styles.checktext }>I Have The Right To Work In The UK</Text>
                            </Body>
                        </ListItem>
                        <Button style={ styles.nextButton } onPress={() => Actions.createHero()} block>
                            <Text>NEXT</Text>
                        </Button>
                </Content>
            </Container>
        )
    }
}

AppRegistry.registerComponent('howItWorks', () => howItWorks);

const styles = StyleSheet.create({
    container:{
        backgroundColor:'#fff',
        borderWidth:0,
        padding:10
    },
    content:{
        backgroundColor:'#fff',
        height:500
    },
    headerTextOne:{
        textAlign:'center',
        marginTop:60
    },
    headerTextTwo:{
        textAlign:'center',
        marginTop:20
    },
    headerTextThree:{
        marginTop:20,
        marginLeft:20,
        marginRight:20,
        textAlign:'center'
    },
    headerTextFour:{
        marginTop:20,
        marginLeft:10,
        marginRight:10,
        textAlign:'center'
    },
    checkbox:{
        borderColor:'#000'
    },
    checktext:{
        color:'#000'
    },
    listitemOne:{
        borderBottomWidth:0,
        marginTop:40,
        marginLeft:30,
    },
    listitem:{
        borderBottomWidth:0,
        marginTop:10,
        marginLeft:30,
    },
    nextButton:{
        borderRadius:0,
        marginTop:20,
        marginLeft:3,
        marginRight:1
    }
})