


var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, TouchableOpacity, ScrollView, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Button, Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import moment from 'moment';

export default class availability extends Component {

    componentDidMount(){

    }

    constructor(props) {
        super(props);
      
        this.state = {
          data: [1,2,3,4,5,6],
          month: moment().format(),
          alpha_month: moment().format('MMMM'),
          current_day: moment().format(),
          current_alpha: moment().format('ddd'),
          current_alpha_pos_1: moment().add(1, 'day').format('ddd'),
          current_alpha_pos_2: moment().add(2, 'days').format('ddd'),
          current_alpha_neg_1: moment().subtract(1, 'day').format('ddd'),
          current_alpha_neg_2: moment().subtract(2, 'days').format('ddd'),
          current_numeric: moment().format('DD'),
          current_numeric_pos_1: moment().add(1, 'day').format('DD'),
          current_numeric_pos_2: moment().add(2, 'days').format('DD'),
          current_numeric_neg_1: moment().subtract(1, 'day').format('DD'),
          current_numeric_neg_2: moment().subtract(2, 'days').format('DD'),



        };

      }

    header = 'My Availability';

    increase(){

        let next_month = moment(this.state.month).add(1, 'month').format();
        let increase_alpha_month = moment(this.state.month).add(1, 'month').format('MMMM');
        this.setState({month:next_month});
        this.setState({alpha_month:increase_alpha_month});

        let increase_days_30 = moment(this.state.current_day).add(30, 'days').format();

        this.setState({current_day:increase_days_30});

        let alpha_days_30 = moment(this.state.current_day).format('ddd');
        let numeric_days_30 = moment(this.state.current_day).format('DD');

        let pos_days_1 = moment(this.state.current_day).add(1, 'day').format('ddd');
        let pos_days_2 = moment(this.state.current_day).add(2, 'days').format('ddd');
        let num_pos_days_1 = moment(this.state.current_day).add(1, 'day').format('DD');
        let num_pos_days_2 = moment(this.state.current_day).add(2, 'days').format('DD');

        let neg_days_1 = moment(this.state.current_day).subtract(1, 'day').format('ddd');
        let neg_days_2 = moment(this.state.current_day).subtract(2, 'days').format('ddd');
        let num_neg_days_1 = moment(this.state.current_day).subtract(1, 'day').format('DD');
        let num_neg_days_2 = moment(this.state.current_day).subtract(2, 'days').format('DD');

        this.setState({current_alpha:alpha_days_30});
        this.setState({current_numeric:numeric_days_30});

        this.setState({current_alpha_pos_1:pos_days_1});
        this.setState({current_alpha_pos_2:pos_days_2});
        this.setState({current_alpha_neg_1:neg_days_1});
        this.setState({current_alpha_neg_2:neg_days_2});

        this.setState({current_numeric_pos_1:num_pos_days_1});
        this.setState({current_numeric_pos_2:num_pos_days_2});
        this.setState({current_numeric_neg_1:num_neg_days_1});
        this.setState({current_numeric_neg_2:num_neg_days_2});
    }

    decrease(){

        let previous_month = moment(this.state.month).subtract(1, 'month').format();
        let decrease_alpha = moment(this.state.month).subtract(1, 'month').format('MMMM');
        this.setState({month:previous_month});
        this.setState({alpha_month:decrease_alpha});

        //Working here end of Monday.
        let decrease_days_30 = moment(this.state.current_day).subtract(30, 'days').format();
        this.setState({current_day:decrease_days_30});

        let alpha_days_30 = moment(this.state.current_day).format('ddd');
        let numeric_days_30 = moment(this.state.current_day).format('DD');

        let pos_days_1 = moment(this.state.current_day).add(1, 'day').format('ddd');
        let pos_days_2 = moment(this.state.current_day).add(2, 'days').format('ddd');
        let num_pos_days_1 = moment(this.state.current_day).add(1, 'day').format('DD');
        let num_pos_days_2 = moment(this.state.current_day).add(2, 'days').format('DD');

        let neg_days_1 = moment(this.state.current_day).subtract(1, 'day').format('ddd');
        let neg_days_2 = moment(this.state.current_day).subtract(2, 'days').format('ddd');
        let num_neg_days_1 = moment(this.state.current_day).subtract(1, 'day').format('DD');
        let num_neg_days_2 = moment(this.state.current_day).subtract(2, 'days').format('DD');

        this.setState({current_alpha:alpha_days_30});
        this.setState({current_numeric:numeric_days_30});

        this.setState({current_alpha_pos_1:pos_days_1});
        this.setState({current_alpha_pos_2:pos_days_2});
        this.setState({current_alpha_neg_1:neg_days_1});
        this.setState({current_alpha_neg_2:neg_days_2});

        this.setState({current_numeric_pos_1:num_pos_days_1});
        this.setState({current_numeric_pos_2:num_pos_days_2});
        this.setState({current_numeric_neg_1:num_neg_days_1});
        this.setState({current_numeric_neg_2:num_neg_days_2});


    }

    render() {

        return (
  
            <Container style={ styles.container }>
                <Content scrollEnabled = {false} style={{flex:1}}>
                    <Grid>
                        <ImageBackground source={require('./assets/header_image.png')} style={styles.bg_image} resizeMode="cover">
                            <Row style={styles.title_row}>
                                <Col style={styles.title_container}>
                                    <Text style={styles.title_text}>{this.header}</Text>
                                </Col>
                            </Row>
                            <Row style={styles.nav_row}>
                                <Col style={styles.nav_container}>
                                    <TouchableOpacity onPress={() => this.decrease()} >
                                        <Image source={require('./assets/back_arrow.png')} style={styles.back_arrow} />
                                    </TouchableOpacity>
                                    <Text style={styles.nav_text}>{this.state.alpha_month}</Text>
                                    <TouchableOpacity onPress={() => this.increase()} >
                                        <Image source={require('./assets/forward_arrow.png')} style={styles.forward_arrow} />
                                    </TouchableOpacity>
                                </Col>
                            </Row>
                        </ImageBackground>
                    </Grid>
                    <Grid style={{flex:1}}>
                        <Row>
                            <Col style={styles.date_container}>
                                <Text style={{fontSize:12,color:'#929292'}}>{this.state.current_alpha_neg_2}</Text>
                                <Text style={{fontSize:20,color:'#929292'}}>{this.state.current_numeric_neg_2}</Text>
                            </Col>
                            <Col style={styles.date_container}>
                                <Text style={{fontSize:12,color:'#929292'}}>{this.state.current_alpha_neg_1}</Text>
                                <Text style={{fontSize:20,color:'#929292'}}>{this.state.current_numeric_neg_1}</Text>
                            </Col>
                            <Col style={styles.date_container}>
                                <Text style={{fontSize:12,color:'#FC5C15'}}>{this.state.current_alpha}</Text>
                                <Text style={{fontSize:20,color:'#FC5C15'}}>{this.state.current_numeric}</Text>
                            </Col>
                            <Col style={styles.date_container}>
                                <Text style={{fontSize:12,color:'#333558'}}>{this.state.current_alpha_pos_1}</Text>
                                <Text style={{fontSize:20,color:'#333558'}}>{this.state.current_numeric_pos_1}</Text>
                            </Col>
                            <Col style={styles.date_container}>
                                <Text style={{fontSize:12,color:'#333558'}}>{this.state.current_alpha_pos_2}</Text>
                                <Text style={{fontSize:20,color:'#333558'}}>{this.state.current_numeric_pos_2}</Text>
                            </Col>
                        </Row>
                        <Row>

                            <Col style={styles.hours_container}>
                                {this.dummy_data()}
                            </Col>
                            <Col style={styles.hours_container}>
                                {this.dummy_data()}
                            </Col>
                            <Col style={styles.hours_container}>
                                {this.dummy_data()}
                            </Col>
                            <Col style={styles.hours_container}>
                                {this.dummy_data()}
                            </Col>
                            <Col style={styles.hours_container}>
                                {this.dummy_data()}
                            </Col>

                        </Row>
                    </Grid>
                </Content>
            </Container>
        )

    }

    dummy_data(){
        return this.state.data.map((data, index) =>{
            return (
                    

                    // Mess more with flex and styles for the boxes friday morning.
                    <View key={index} style={{
                        height:48,
                        width:'80%',
                        marginBottom:1,
                        borderRadius:5,
                        backgroundColor:'#E6E6E6',
                        }}>
                    </View>

            )
        }) 
    }

    
}

AppRegistry.registerComponent('availability', () => availability);

const styles = StyleSheet.create({

    container:{
        borderWidth:0,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor:'#F5F6F8'
    },
    bg_image:{
        flex: 1,
        width: null,
        height: null,
    },
    title_row:{
        height:60
    },
    title_container:{
        height:50,
        paddingTop:50,
        flex:1,
        justifyContent:'center'
    },
    title_text:{
        textAlign:'center',
        fontSize:24,
        color:'white'
    },
    nav_row:{
        height:50
    },
    nav_container:{
        flex:1, 
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row'
    },
    back_arrow:{
        marginLeft:20
    },
    nav_text:{
        color:'white'
    },
    forward_arrow:{
        marginRight:20
    },
    date_container:{
        height:50,
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5F6F8',

    },
    hours_container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5F6F8',



    },
    hours_container_half:{
        flex:.10,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#F5F6F8',
        borderWidth:1,
        borderColor:'black'
    }

})