



var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, TextInput, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, ListItem, Body, CheckBox, StyleProvider, H1 } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LinearGradient from 'react-native-linear-gradient';

export default class forgot_pass extends Component {

    componentWillMount(){
        AsyncStorage.getItem("forgot_password").then((value) => {
             this.setState({
                 resetpassword: value
             });
            }).done();
    }

    constructor(props){
        super(props)

        this.state = {
            emailAddress:'',
            status: false,
            error: false,
            email_message:''
            
        }
    }

    reset_pass(){
        
        let self = this;

            axios.post(self.state.resetpassword, {
                email: self.state.emailAddress,
                type: 'hero'
            }).then((response) => {
                console.log(response);
                var message = '*' + response.data.response.message;
                this.setState({email_message: message})

                setTimeout(function()
                    { Actions.login(); },     
                3000);
            })

            if(self.state.status == false){

                this.setState({status: true});

            }else{
                this.setState({status: false})
            }

    }

    render(){
        return(
            <ImageBackground source={require('./assets/bg_320.png')} style={{flex: 1,width: null,height: null,}} resizeMode="cover">
            <Container>
                <Content style={ styles.content}>
                    <Button transparent light style={styles.back_arrow_container} onPress={() => Actions.login()}>
                            <Image source={require('./assets/back_arrow.png')} />
                    </Button>
                    <H1 style={ styles.h1 }>Forgotten password</H1>
                    <Text style={ styles.label }>Type in your email address to recover your password</Text>

                    <Grid>
                        <Row>
                            <Col style={{padding:5}}>
                                <TextInput style={styles.input_style} placeholder='Email Address' placeholderTextColor='#B3B3B3' onChangeText={(text) => this.setState({emailAddress:text})} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Text style={styles.error}>{ this.state.email_message }</Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{padding:5}}>
                                <LinearGradient start={{x: 0, y: 0.75}} end={{x: 1, y: 0.25}} colors={['#ffb40b', '#fc4615']} style={styles.resetGradient}>
                                    <TouchableOpacity style={styles.button} onPress={() => this.reset_pass()} >
                                        <Text style={styles.buttonText}>Send</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            </Col>
                            </Row>
                    </Grid>
                </Content>
            </Container>
            </ImageBackground>
        )
    }

}

AppRegistry.registerComponent('forgot_pass', () => forgot_pass);

const styles = StyleSheet.create({

    container:{
        backgroundColor:'#000',
        borderWidth:0
    },
    input_style:{
        height:40,
        backgroundColor:'#5d5e7f',
        borderRadius:5,
        paddingLeft:10,
        color:'white',
        marginTop:40
    },
    header:{
        backgroundColor:'#000',
        height:110,
        borderBottomWidth:0
    },
    content:{
        height:100,
        padding:10
    },
    h1:{
        color:'#fff',
        textAlign:'center',
        marginTop:30
    },
    label:{
        color:'#fff',
        textAlign:'center',
        paddingLeft:40,
        paddingRight:40,
        paddingTop:10
    },
    resetGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
      },
    button:{
        height:40
    },
    buttonText:{
        marginTop:10,
        textAlign:'center',
        color:'white'
    },
    success:{
        textAlign:'center',
        color:'white'
    },
    error:{
        textAlign:'left',
        color:'#2af5ff',
        marginTop:5,
        marginLeft:5

    },
    back_arrow_container:{
        width:60,
        height:40,
        top:35,
        left:5,
        position:'absolute',
        zIndex:2,
    },
})
