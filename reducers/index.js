



import { AVAILABLE_DAYS } from "../constants/action-types";

import { GET_DAYS } from "../constants/action-types";

const initialState = {
  available_days: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case AVAILABLE_DAYS:
        return { ...state, available_days: [...state.available_days, action.payload] };
    default:
      return state;
  }
};
export default rootReducer